<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_OBJ,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mongodb'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_banesco' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_banesco', 'localhost'),
            'port' => env('DB_PORT_banesco', '3306'),
            'database' => env('DB_DATABASE_banesco', 'forge'),
            'username' => env('DB_USERNAME_banesco', 'forge'),
            'password' => env('DB_PASSWORD_banesco', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_affglo' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_affglo', 'localhost'),
            'port' => env('DB_PORT_affglo', '3306'),
            'database' => env('DB_DATABASE_affglo', 'forge'),
            'username' => env('DB_USERNAME_affglo', 'forge'),
            'password' => env('DB_PASSWORD_affglo', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_bac' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_bac', 'localhost'),
            'port' => env('DB_PORT_bac', '3306'),
            'database' => env('DB_DATABASE_bac', 'forge'),
            'username' => env('DB_USERNAME_bac', 'forge'),
            'password' => env('DB_PASSWORD_bac', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_cableonda' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_cableonda', 'localhost'),
            'port' => env('DB_PORT_cableonda', '3306'),
            'database' => env('DB_DATABASE_cableonda', 'forge'),
            'username' => env('DB_USERNAME_cableonda', 'forge'),
            'password' => env('DB_PASSWORD_cableonda', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],


        'mysql_cirsa' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_cirsa', 'localhost'),
            'port' => env('DB_PORT_cirsa', '3306'),
            'database' => env('DB_DATABASE_cirsa', 'forge'),
            'username' => env('DB_USERNAME_cirsa', 'forge'),
            'password' => env('DB_PASSWORD_cirsa', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_cochez' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_cochez', 'localhost'),
            'port' => env('DB_PORT_cochez', '3306'),
            'database' => env('DB_DATABASE_cochez', 'forge'),
            'username' => env('DB_USERNAME_cochez', 'forge'),
            'password' => env('DB_PASSWORD_cochez', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_credicorp' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_credicorp', 'localhost'),
            'port' => env('DB_PORT_credicorp', '3306'),
            'database' => env('DB_DATABASE_credicorp', 'forge'),
            'username' => env('DB_USERNAME_credicorp', 'forge'),
            'password' => env('DB_PASSWORD_credicorp', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],


        'mysql_banescobl' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_banescobl', 'localhost'),
            'port' => env('DB_PORT_banescobl', '3306'),
            'database' => env('DB_DATABASE_banescobl', 'forge'),
            'username' => env('DB_USERNAME_banescobl', 'forge'),
            'password' => env('DB_PASSWORD_banescobl', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],



        'mysql_cwcbusiness' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_cwcbusiness', 'localhost'),
            'port' => env('DB_PORT_cwcbusiness', '3306'),
            'database' => env('DB_DATABASE_cwcbusiness', 'forge'),
            'username' => env('DB_USERNAME_cwcbusiness', 'forge'),
            'password' => env('DB_PASSWORD_cwcbusiness', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_emarket-paris' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_emarket-paris', 'localhost'),
            'port' => env('DB_PORT_emarket-paris', '3306'),
            'database' => env('DB_DATABASE_emarket-paris', 'forge'),
            'username' => env('DB_USERNAME_emarket-paris', 'forge'),
            'password' => env('DB_PASSWORD_emarket-paris', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_interbank' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_interbank', 'localhost'),
            'port' => env('DB_PORT_interbank', '3306'),
            'database' => env('DB_DATABASE_interbank', 'forge'),
            'username' => env('DB_USERNAME_interbank', 'forge'),
            'password' => env('DB_PASSWORD_interbank', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_mapfre' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_mapfre', 'localhost'),
            'port' => env('DB_PORT_mapfre', '3306'),
            'database' => env('DB_DATABASE_mapfre', 'forge'),
            'username' => env('DB_USERNAME_mapfre', 'forge'),
            'password' => env('DB_PASSWORD_mapfre', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_masmedan' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_masmedan', 'localhost'),
            'port' => env('DB_PORT_masmedan', '3306'),
            'database' => env('DB_DATABASE_masmedan', 'forge'),
            'username' => env('DB_USERNAME_masmedan', 'forge'),
            'password' => env('DB_PASSWORD_masmedan', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_metrobank' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_metrobank', 'localhost'),
            'port' => env('DB_PORT_metrobank', '3306'),
            'database' => env('DB_DATABASE_metrobank', 'forge'),
            'username' => env('DB_USERNAME_metrobank', 'forge'),
            'password' => env('DB_PASSWORD_metrobank', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_panacredit' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_panacredit', 'localhost'),
            'port' => env('DB_PORT_panacredit', '3306'),
            'database' => env('DB_DATABASE_panacredit', 'forge'),
            'username' => env('DB_USERNAME_panacredit', 'forge'),
            'password' => env('DB_PASSWORD_panacredit', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_petroautos' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_petroautos', 'localhost'),
            'port' => env('DB_PORT_petroautos', '3306'),
            'database' => env('DB_DATABASE_petroautos', 'forge'),
            'username' => env('DB_USERNAME_petroautos', 'forge'),
            'password' => env('DB_PASSWORD_petroautos', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_ricardoperez' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_ricardoperez', 'localhost'),
            'port' => env('DB_PORT_ricardoperez', '3306'),
            'database' => env('DB_DATABASE_ricardoperez', 'forge'),
            'username' => env('DB_USERNAME_ricardoperez', 'forge'),
            'password' => env('DB_PASSWORD_ricardoperez', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_scotiabank' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_scotiabank', 'localhost'),
            'port' => env('DB_PORT_scotiabank', '3306'),
            'database' => env('DB_DATABASE_scotiabank', 'forge'),
            'username' => env('DB_USERNAME_scotiabank', 'forge'),
            'password' => env('DB_PASSWORD_scotiabank', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mysql_unibank' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_unibank', 'localhost'),
            'port' => env('DB_PORT_unibank', '3306'),
            'database' => env('DB_DATABASE_unibank', 'forge'),
            'username' => env('DB_USERNAME_unibank', 'forge'),
            'password' => env('DB_PASSWORD_unibank', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],


        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'mongodb_banesco' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_banesco', 'localhost'),
            'port'     => env('DB_PORT_MONGO_banesco', 27017),
            'database' => env('DB_DATABASE_MONGO_banesco'),
            'username' => env('DB_USERNAME_MONGO_banesco'),
            'password' => env('DB_PASSWORD_MONGO_banesco')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],

        'mongodb_cochez' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_cochez', 'localhost'),
            'port'     => env('DB_PORT_MONGO_cochez', 27017),
            'database' => env('DB_DATABASE_MONGO_cochez'),
            'username' => env('DB_USERNAME_MONGO_cochez'),
            'password' => env('DB_PASSWORD_MONGO_cochez')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],

        'mongodb_bac' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_bac', 'localhost'),
            'port'     => env('DB_PORT_MONGO_bac', 27017),
            'database' => env('DB_DATABASE_MONGO_bac'),
            'username' => env('DB_USERNAME_MONGO_bac'),
            'password' => env('DB_PASSWORD_MONGO_bac')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],

        'mongodb_cwcbusiness' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_cwcbusiness', 'localhost'),
            'port'     => env('DB_PORT_MONGO_cwcbusiness', 27017),
            'database' => env('DB_DATABASE_MONGO_cwcbusiness'),
            'username' => env('DB_USERNAME_MONGO_cwcbusiness'),
            'password' => env('DB_PASSWORD_MONGO_cwcbusiness')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],

        'mongodb_interbank' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_interbank', 'localhost'),
            'port'     => env('DB_PORT_MONGO_interbank', 27017),
            'database' => env('DB_DATABASE_MONGO_interbank'),
            'username' => env('DB_USERNAME_MONGO_interbank'),
            'password' => env('DB_PASSWORD_MONGO_interbank')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],

        'mongodb_mapfre' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_mapfre', 'localhost'),
            'port'     => env('DB_PORT_MONGO_mapfre', 27017),
            'database' => env('DB_DATABASE_MONGO_mapfre'),
            'username' => env('DB_USERNAME_MONGO_mapfre'),
            'password' => env('DB_PASSWORD_MONGO_mapfre')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],

        'mongodb_metrobank' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_metrobank', 'localhost'),
            'port'     => env('DB_PORT_MONGO_metrobank', 27017),
            'database' => env('DB_DATABASE_MONGO_metrobank'),
            'username' => env('DB_USERNAME_MONGO_metrobank'),
            'password' => env('DB_PASSWORD_MONGO_metrobank')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],

        'mongodb_petroautos' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_petroautos', 'localhost'),
            'port'     => env('DB_PORT_MONGO_petroautos', 27017),
            'database' => env('DB_DATABASE_MONGO_petroautos'),
            'username' => env('DB_USERNAME_MONGO_petroautos'),
            'password' => env('DB_PASSWORD_MONGO_petroautos')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],


        'mongodb_unibank' => [
            'driver'   => 'mongodb',
            'host'     => env('DB_HOST_MONGO_unibank', 'localhost'),
            'port'     => env('DB_PORT_MONGO_unibank', 27017),
            'database' => env('DB_DATABASE_MONGO_unibank'),
            'username' => env('DB_USERNAME_MONGO_unibank'),
            'password' => env('DB_PASSWORD_MONGO_unibank')
            //'options' => [
            //'database' => 'admin' // sets the authentication database required by mongo 3
            //]
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => false,

        'default' => [
            'host' => env('REDIS_HOST', 'localhost'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
