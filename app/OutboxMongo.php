<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

// use Moloquent;

use DB;

class OutboxMongo extends Moloquent
{

        public $connection = 'mongodb_bac';

        protected $primaryKey = '_id';

        protected $keyType = "string";

        protected $collection = 'mensaje';

        public function __construct(){

        	$this->connection = 'mongodb_bac';
        	$this->primaryKey = '_id';
        	$this->collection = 'mensaje';

        }

        public function buscar_datos ($outbox_id)
        {
    		// $consulta = OutboxMongo::All()->limit(1)->get();
    		// $consulta = DB::collection('mensaje')->limit(1)->get();
    		// $consulta = OutboxMongo::where('_id',$outbox_id)->limit(1)->get();
    		$consulta = DB::collection('mensaje')->take(10);
            // dd($consulta);
            return $consulta;
    	}

}
