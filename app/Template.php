<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public $connection = 'mysql_interbank';

	protected $table = 'template';

	protected $primaryKey = 'id';

	// protected $incrementing = false;

	// protected $keyType = "string";
	protected $fillable = [
        'id', 'name', 'subject'
    ];


    public function buscar_datos ($name){

    		$consulta = Template::where('name',$name)
    		->limit(1)
            ->get();

            return $consulta;
    }


    

}
