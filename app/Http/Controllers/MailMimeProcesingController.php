<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\OutboxMySql;

use App\Template;

use App\OutboxLog;

// use App\OutboxMongo;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Hash;

use DB;

use AltThree\Bus\Dispatcher;


class MailMimeProcesingController extends Controller
{

    /**
     * Hace la conexion al modelo de mysql para la table template
     * @param string  $name
     * @return string
     */

    public function consultas_basedatos_template($My,$name)
    {
        $outboxIsnMyTem = new Template;       
        $outboxIsnMyTem->connection = $My;
        $consulta = $outboxIsnMyTem->buscar_datos($name);
        return $consulta;
    }


    /**
     * Hace la conexion al modelo de mysql y mongo para traer la data
     * @param string  $My
     * @param string  $Mo
     * @param array   $fecha_buscar
     * @param string  $name_file
     * @return array
     */

    public function consultas_basedatos_outbox_log_status($My,$outbox_id)
    {
        $outboxIsnMyLog = new OutboxLog;       
        $outboxIsnMyLog->connection = $My;
        $consulta = $outboxIsnMyLog->buscar_datos($outbox_id);
        return $consulta;
    }

    /**
     * Hace la conexion al modelo de mongo para traer la data
     * @param string  $Mo
     * @param string  $outbox_id
     * @return array
     */

    public function consultas_basedatos_mongo($Mo,$outbox_id)
    {

        $outboxIsnMo = new OutboxMongo;
        $outboxIsnMo->connection = $Mo;
        $consulta = $outboxIsnMo->buscar_datos($outbox_id);
        return $consulta;
    }
    /**
     * Hace la conexion al modelo de mysql  para traer la data
     * @param string  $My
     * @param string  $Mo
     * @param array   $fecha_buscar
     * @param string  $name_file
     * @return array
     */

    public function consultas_basedatos($My,$fecha_buscar,$name_file)
    {

        echo date('Y-m-d H:i:s').$name_file."\n";

        $outboxIsnMy = new OutboxMySql;
        
        $outboxIsnMy->connection = $My;
        $consulta = $outboxIsnMy->buscar_datos($fecha_buscar);
        return $consulta;
    }

    /**
     * Hace la conexion al modelo de mysql y mongo para traer la data
     * @param datetime  $fecha_desde
     * @param datetime  $fecha_hasta
     * @return array
     */
    public function descomponer($fecha_desde,$fecha_hasta)
    {

       
        $outboxMy="";
        $outboxMo="";

        echo date('Y-m-d H:i:s')."\n";
        /*
        *
        * Banesco
        */  
            $outbox=array();

            $outboxMy = 'mysql_banesco';
            $outboxMo = 'mongodb_banesco';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);
            $name_file = "banesco";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            
            if($outbox->count()>0){
                $datos_banesco = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);}
            else{
                $datos_banesco = array();
            }


        /*
        *
        * Banesco
        */   

        /*
        *
        * affglo
        */  
            unset($outbox);
            $outboxMy = 'mysql_affglo';
            $outboxMo = 'mongodb_banesco';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            $name_file = "affglo";
            if($outbox->count()>0){
                $datos_affglo = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);}
            else{
                $datos_affglo = array();
            }

            

        /*
        *
        * affglo
        */   

        /*
        *
        * bac
        */  
            unset($outbox);
            $outboxMy = 'mysql_bac';
            $outboxMo = 'mongodb_bac';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "bac";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_bac = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_bac = array();
            }

            

        /*
        *
        * bac
        */


        /*
        *
        * banescobl
        */  
            unset($outbox);
            $outboxMy = 'mysql_banescobl';
            $outboxMo = 'mongodb_banescobl';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "banescobl";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);

            if($outbox->count()>0){
                $datos_banescobl = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_banescobl = array();
            }

            

        /*
        *
        * banescobl
        */

        /*
        *
        * cableonda
        */  
            unset($outbox);
            $outboxMy = 'mysql_cableonda';
            $outboxMo = 'mongodb_cableonda';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "cableonda";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
             if($outbox->count()>0){
                $datos_cableonda = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_cableonda = array();
            }

            

        /*
        *
        * cableonda
        */

        /*
        *
        * cirsa
        */  
            unset($outbox);
            $outboxMy = 'mysql_cirsa';
            $outboxMo = 'mongodb_cirsa';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "cirsa";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_cirsa = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_cirsa = array();
            }
            

        /*
        *
        * cirsa
        */


        /*
        *
        * cochez
        */  
            unset($outbox);
            $outboxMy = 'mysql_cochez';
            $outboxMo = 'mongodb_cochez';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "cochez";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_cochez = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_cochez = array();
            }
            

        /*
        *
        * cochez
        */

        /*
        *
        * credicorp
        */  
            unset($outbox);
            $outboxMy = 'mysql_credicorp';
            $outboxMo = 'mongodb_credicorp';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "credicorp";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_credicorp = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_credicorp = array();
            }
            

        /*
        *
        * credicorp
        */


        /*
        *
        * cwcbusiness
        */  
            unset($outbox);
            $outboxMy = 'mysql_cwcbusiness';
            $outboxMo = 'mongodb_cwcbusiness';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "cwcbusiness";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_cwcbusiness = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_cwcbusiness = array();
            }
            

        /*
        *
        * cwcbusiness
        */


        /*
        *
        * emarket-paris
        */  
            unset($outbox);
            $outboxMy = 'mysql_emarket-paris';
            $outboxMo = 'mongodb_emarket-paris';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "emarket_paris";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_emarket_paris = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_emarket_paris = array();
            }
            

        /*
        *
        * emarket-paris
        */


        /*
        *
        * interbank
        */  
            unset($outbox);
            $outboxMy = 'mysql_interbank';
            $outboxMo = 'mongodb_interbank';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "interbank";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_interbank = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_interbank = array();
            }
            

        /*
        *
        * interbank
        */


        /*
        *
        * mapfre
        */  
            unset($outbox);
            $outboxMy = 'mysql_mapfre';
            $outboxMo = 'mongodb_mapfre';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "mapfre";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_mapfre = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_mapfre = array();
            }
            

        /*
        *
        * mapfre
        */


        /*
        *
        * masmedan
        */  
            unset($outbox);
            $outboxMy = 'mysql_masmedan';
            $outboxMo = 'mongodb_masmedan';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "masmedan";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_masmedan = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_masmedan = array();
            }
            

        /*
        *
        * masmedan
        */

        /*
        *
        * metrobank
        */  
            unset($outbox);
            $outboxMy = 'mysql_metrobank';
            $outboxMo = 'mongodb_metrobank';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "metrobank";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_metrobank = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_metrobank = array();
            }
            

        /*
        *
        * metrobank
        */


        /*
        *
        * panacredit
        */  
            unset($outbox);
            $outboxMy = 'mysql_panacredit';
            $outboxMo = 'mongodb_panacredit';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "panacredit";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_panacredit = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_panacredit = array();
            }
            

        /*
        *
        * panacredit
        */

        /*
        *
        * petroautos
        */  
            unset($outbox);
            $outboxMy = 'mysql_petroautos';
            $outboxMo = 'mongodb_petroautos';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "petroautos";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_petroautos = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_petroautos = array();
            }
            

        /*
        *
        * petroautos
        /

        

        /*
        *
        * ricardoperez
        */  
            unset($outbox);
            $outboxMy = 'mysql_ricardoperez';
            $outboxMo = 'mongodb_ricardoperez';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "ricardoperez";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_ricardoperez = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_ricardoperez = array();
            }
            

        /*
        *
        * ricardoperez
        /

        /*
        *
        * scotibank
        */  
            unset($outbox);
            $outboxMy = 'mysql_scotiabank';
            $outboxMo = 'mongodb_scotianbank';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "scotibank";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_scotibank = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_scotibank = array();
            }
            

        /*
        *
        * scotibank
        /

        /*
        *
        * unibank
        */  
            unset($outbox);
            $outboxMy = 'mysql_unibank';
            $outboxMo = 'mongodb_unibank';
            $fecha_buscar = array($fecha_desde, $fecha_hasta);           
            $name_file = "unibank";
            $outbox = $this->consultas_basedatos($outboxMy,$fecha_buscar,$name_file);
            if($outbox->count()>0){
                $datos_unibank = $this->procesamiento($outbox,$name_file,$outboxMy,$outboxMo);
            }
            else{
                $datos_unibank = array();
            }
            

        /*
        *
        * unibank
        /


        /*
        *
        *CSV
        */
        

            $array = array_collapse([$datos_cableonda,$datos_banescobl,
            $datos_affglo,$datos_banesco,
            $datos_cirsa,$datos_unibank,
            $datos_bac,$datos_scotibank,
            $datos_petroautos,$datos_ricardoperez,
            $datos_panacredit,$datos_metrobank,
            $datos_masmedan,$datos_mapfre,
            $datos_cwcbusiness,$datos_cochez,
            $datos_emarket_paris,$datos_interbank,
            $datos_credicorp]);

            

            $name_file = "desde_".$fecha_desde."_hasta_".$fecha_hasta;
            $r = $this->generador_csv($array,$name_file);

      
        /*
        *
        *CSV
        */

        echo date('Y-m-d H:i:s')."\n";
    }

    /**
     * Limpiar el from
     * @param strim  $from
     * @return string
     */
    public function buscar_from ($from)
    {
        $find="<";

        if(strpos($from, $find)>0)
        {
            $posi = strpos($from, $find)+1;
            $find=">";
            $posf = strpos($from, $find)-2;
            $posf = strlen(substr($from,$posi,strlen($from)))-1;
            return substr($from,$posi,$posf);
        }
        else
        {
            return $from;
        }
    }

     /**
     * Cuenta las variables
     * @param string  $P1
     * @param string  $P2
     * @param string  $P3
     * @param string  $P4
     * @param string  $P5
     * @param string  $P6
     * @param string  $P7
     * @param string  $P8
     * @param string  $P9
     * @param string  $P10
     * @return int
     */
    public function cantidad_var($P1,$P2,$P3,$P4,$P5,$P6,$P7,$P8,$P9,$P10)
    {
        $contador_var=0;

        if($P1<>"" && $P1<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P2<>"" && $P2<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P3<>"" && $P3<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P4<>"" && $P4<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P5<>"" && $P5<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P6<>"" && $P6<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P7<>"" && $P7<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P8<>"" && $P8<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P9<>"" && $P9<>"null"){
            $contador_var=$contador_var+1;
        }

        if($P10<>"" && $P10<>"null"){
            $contador_var=$contador_var+1;
        }

        return $contador_var;
    }

    /**
     * Procesa los mensajesde outbox
     * @param arrays $outbox 
     * @param string $name_file
     * @return arrys
     */
    public function procesamiento($outbox,$name_file,$outboxMy,$outboxMo)
    {
            $con=1;
            $cantidad_imaganes = 0;

            echo $name_file."\n";
            
            $datos =array();

            foreach ($outbox as $key => $value){                   
                    
                    echo ".";
                    
                    Storage::put('archivo_mensajes/'.$con.'.html', $value->message);
                    $size = Storage::size('archivo_mensajes/'.$con.'.html');


                    /**
                    *
                    * Procesing Messages
                    */
                    
                    $a = htmlentities($value->message);
                    $words = implode(",",array_unique(str_word_count(strip_tags(str_replace("'","",str_replace("&uacute;","ú",str_replace("&oacute;","ó",str_replace("&iacute;","í",str_replace("&eacute;","é",str_replace("&aacute;","á",str_replace("&nbsp;", " ", html_entity_decode($a))))))))),2,'áéíóú')));

                    $cant_words = count(array_unique(str_word_count(strip_tags(str_replace("'","",str_replace("&uacute;","ú",str_replace("&oacute;","ó",str_replace("&iacute;","í",str_replace("&eacute;","é",str_replace("&aacute;","á",str_replace("&nbsp;", " ", html_entity_decode($a))))))))),2,'áéíóú')));

                    /**
                    *
                    * Procesing Messages
                    */

                    

                    /**
                    *
                    *Imagenes procesamiento
                    **/
            
                    $i['images'] = strip_tags($value->message, '<img>');
                    $i['images'] = strip_tags($i['images'], '<img>');
                    $i['images']  = explode("src=", str_replace("&quot;", "",htmlspecialchars(stripslashes( $i['images'] ))));
                    
                    $cantidad_imaganes=0;
                    $size_img=0;

                    for ($x=0; $x <= count($i['images']); $x++) { 
                        if(isset($i['images'][$x])){
                            
                            if (preg_match("/\bpng\b/i", $i['images'][$x] )) {
                                $cantidad_imaganes = $cantidad_imaganes+1;

                                $i["img"][$x]['nameurlpic'] = substr($i['images'][$x],0,strpos($i['images'][$x],'png',0)+3);
                                Storage::put('imagenes/'.$x.'.png', substr($i['images'][$x],0,strpos($i['images'][$x],'png',0)+3));

                                $exists = Storage::disk('local')->exists('imagenes/'.$x.'.png');
                                if($exists){
                                    $size_img = Storage::size('imagenes/'.$x.'.png');
                                    $i["img"][$x]['img_size'] = $size_img;
                                    Storage::delete('imagenes/'.$x.'.png');
                                }

                            } 
                            if (preg_match("/\bjpg\b/i", $i['images'][$x] )) {
                                $cantidad_imaganes = $cantidad_imaganes+1;
                                $i["img"][$x]['nameurlpic'] = substr($i['images'][$x],0,strpos($i['images'][$x],'jpg',0)+3);
                                Storage::put('imagenes/'.$x.'.jpg', substr($i['images'][$x],0,strpos($i['images'][$x],'png',0)+3));

                                $exists = Storage::disk('local')->exists('imagenes/'.$x.'.jpeg');
                                if($exists){
                                    $size_img = Storage::size('imagenes/'.$x.'.jpg');
                                    $i["img"][$x]['img_size'] = $size_img;
                                    Storage::delete('imagenes/'.$x.'.jpg');
                                }

                            } 
                            if (preg_match("/\bjpeg\b/i", $i['images'][$x] )) {
                                $cantidad_imaganes = $cantidad_imaganes+1;
                                $i["img"][$x]['nameurlpic'] = substr($i['images'][$x],0,strpos($i['images'][$x],'jpeg',0)+3);
                                Storage::put('imagenes/'.$x.'.jpeg', substr($i['images'][$x],0,strpos($i['images'][$x],'png',0)+3));

                                $exists = Storage::disk('local')->exists('imagenes/'.$x.'.jpeg');
                                if($exists){
                                    $size_img = Storage::size('imagenes/'.$x.'.jpeg');
                                    $i["img"][$x]['img_size'] = $size_img;
                                    Storage::delete('imagenes/'.$x.'.jpeg');
                                }

                            }

                            if (preg_match("/\bgif\b/i", $i['images'][$x] )) {
                                $cantidad_imaganes = $cantidad_imaganes+1;
                                $i["img"][$x]['nameurlpic'] = substr($i['images'][$x],0,strpos($i['images'][$x],'gif',0)+3);
                                Storage::put('imagenes/'.$x.'.gif', substr($i['images'][$x],0,strpos($i['images'][$x],'gif',0)+3));

                                $exists = Storage::disk('local')->exists('imagenes/'.$x.'.gif');
                                if($exists){
                                    $size_img = Storage::size('imagenes/'.$x.'.gif');
                                    $i["img"][$x]['img_size'] = $size_img;
                                    Storage::delete('imagenes/'.$x.'.gif');
                                }
                            } 

                            $size_img=$size_img+$size_img;
                        }
                    }

                    /**
                    *
                    *Imagenes procesamiento
                    **/

                    unset($i['images']);

                    if($value->template_name<>"" && $value->template_name<>"free" && $value->template_name<>"Free"){
                        $var_cant = $this->cantidad_var($value->P1,$value->P2,$value->P3,$value->P4,$value->P5,$value->P6,$value->P7,$value->P8,$value->P9,$value->P10);
                    }else{
                        $var_cant = 0;
                    }

                    if($var_cant==" "){$var_cant=0;}

                    $client=$name_file;
                    
                    if($value->status_id == 4 || $value->status_id == 43 || $value->status_id==13 || $value->status_id==6){

                        $outbox_log = $this->consultas_basedatos_outbox_log_status($outboxMy,$value->id);

                        foreach ($outbox_log as $key => $outbox_log_value) {
                            $bounce_error=$outbox_log_value->event;
                        }                        

                         // $bounce_error="";

                    }else{
                        $bounce_error="";
                    }

                    /**
                    *
                    *Procecing of Subject
                    */
                    
                    
                        $subject_words="";
                        $subject_cant_words=0;

                        $created_datetime = $value->created;
                        $name = $value->template_name;
                        if($value->type=='free' || $value->type=="null" || $value->type==" ")
                        {
                            // $subject='Sin/Subject';
                            $subject_words="";
                            $subject_cant_words=0;

                        }
                        else
                        {
                            if($outboxMo=="mongodb_bac")
                            {
                                // $template_subject =  $this->consultas_basedatos_mongo($outboxMo,$value->id);
                                // foreach ($template_subject as $key => $value_template_subject) 
                                // {
                                //     $subject_words = implode(",",array_unique(explode(" ",$value_template_subject->subject)));
                                //     $subject = array_unique(explode(" ",$value_template_subject->subject));
                                //     $subject_cant_words = count($subject);
                                // }

                                
                                $subject_words = implode(",",array_unique(explode(" ","Detalle de Transacciones")));
                                $subject = array_unique(explode(" ","Detalle de Transacciones"));
                                $subject_cant_words = count($subject);

                                


                            }
                            else
                            {
                                $template_subject =  $this->consultas_basedatos_template($outboxMy,$name);
                                foreach ($template_subject as $key => $value_template_subject) 
                                {
                                    $subject_words = implode(",",array_unique(explode(" ",$value_template_subject->subject)));
                                    $subject = array_unique(explode(" ",$value_template_subject->subject));
                                    $subject_cant_words = count($subject);
                                }
                            }
                        }

                    /**
                    *
                    *Procecing of Subject
                    */

                    $from = $value->from;
                    $from = $this->buscar_from($from);
                    $to = $value->destination;

                    $status = DB::table('status')->where('id', $value->status_id)->get()->first();
                    $sta = $status->label;

                    $nuevafecha = strtotime ( '-5 hour' , strtotime ( $value->created ) ) ;
                    $nuevafecha = date ( 'Y-m-j H:i:s' , $nuevafecha );

                    $datos[$value->id]['from'] = $from;
                    $datos[$value->id]['to'] = $to;
                    $datos[$value->id]['date'] = $nuevafecha;
                    $datos[$value->id]['id'] = $value->id;
                    $datos[$value->id]['subject_words'] = $subject_words;
                    $datos[$value->id]['subject_cant_words'] = $subject_cant_words;
                    $datos[$value->id]['size_file'] = $size;
                    $datos[$value->id]['body_words'] = $words;
                    $datos[$value->id]['cant_words'] = $cant_words;
                    $datos[$value->id]['body_content_cant_img'] = $cantidad_imaganes;
                    $datos[$value->id]['body_content_size_img'] = $size_img;
                    $datos[$value->id]['referencia_platilla'] = $value->template_name;
                    // $datos[$value->id]['words_content_body'] = $var_cant;
                    $datos[$value->id]['bounce'] = $sta;
                    $datos[$value->id]['bounce_error'] = $bounce_error;
                    $datos[$value->id]['client'] = $client;
                    Storage::delete('archivo_mensajes/'.$con.'.html');
                    $con=$con+1;

            }
           
            return $datos;
    }

    /**
     * Generar archivos csv
     *
     * @param  array  $datos
     * @param  string  $namefile
     * @return int
     */
    public function generador_csv($datos,$namefile)
    {
        \Excel::create($namefile, function($excel) use( $datos ) {
                $excel->sheet('Estadisticas', function($sheet) use( $datos ) {
                    $sheet->fromArray($datos);
                });
        })->store('csv');

        echo "esta_aqui - > /var/www/html/enginemail/storage/exports/".$namefile.".csv"."\n";

        return 1;
    }
}
