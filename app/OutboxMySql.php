<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutboxMySql extends Model
{
	public $connection = 'mysql_interbank';

	protected $table = 'outbox';

	protected $primaryKey = 'id';

	// protected $incrementing = false;

	protected $keyType = "string";


	protected $fillable = [
        'id', 'from', 'destination', 'carbon_copy', 'type', 'template_name', 'message', 'attachments', 'app_source', 'status_id', 
        'user_id', 'past_change', 'P1', 'P2','P3','P4', 'P5', 'P6', 'P7','P8', 'P9','P10', 'external_id','retried','numeric_id',
        'connection','campaign_id', 'replyto','created','tags','carrier','campaign_name','status_name','center_id','center_name',
        'user_name'
    ];

    public function buscar_datos ($fecha_buscar){

    		$consulta = OutboxMySql::where('type','email')
            ->whereBetween('created',$fecha_buscar)
            ->orderBy('created')
            ->get();

            return $consulta;
    }

}
