<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutboxLog extends Model
{
    public $connection = 'mysql_interbank';

	protected $table = 'outbox_log';

	// protected $primaryKey = 'id';


	protected $fillable = [
        'id', 'outbox_id', 'event_date', 'event'
    ];

    public function buscar_datos ($outbox_id){

    		$consulta = OutboxLog::where('outbox_id', $outbox_id)
    		->orWhere(function ($query){$query
    			->orWhere('event','like','Bounce%')
    			->orWhere('event','like','Failed - 5%')
    			->orWhere('event','like','Failed-Invalid%')
    			->orWhere('event','like','Dropped%');     		
    		})
            ->limit(1)
            ->get();

            return $consulta;
    }

}
