<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
class GeneraCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'genera:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
       $this->info("Generando CSV !!");       
       $p = new \App\Http\Controllers\MailMimeProcesingController();     
       $i=7;
       while ($i<=30)
       {
               $desde1='2016-09-'.$i.' 00:00:00';
               $hasta1='2016-09-'.$i.' 11:59:59';
               $desde2='2016-09-'.$i.' 12:00:00';
               $hasta2='2016-09-'.$i.' 23:59:59';

	       $p->descomponer($desde1,$hasta1);
	       $p->descomponer($desde2,$hasta2); 
               $i++;
      }

    }
}
